﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    public static DialogueManager instance;
    public Text nameText;
    public Text dialogueText;
    Queue<string> sentences;
    public Animator animator;
    public GameObject choices;

    public Sprite unpressedBtn;
    public Sprite pressedBtn;

    //Dialogue currentDialogue;

    ////reputation bar
    //public float honor;
    //public float maxHonor = 100.0f;
    //public Transform honorBar;
    //public Image honorFill;

	// Use this for initialization
	void Start () {
        sentences = new Queue<string>();
        loadText("init.txt");
        instance = this;
        //honor = 50.0f;
        //honorFill.fillAmount = honor / maxHonor;
	}


    public void loadText(string sourceFile) //call whenever loading a new text file
    {
        TextAsset txtAssets = (TextAsset)Resources.Load(sourceFile);
        string txtContents = txtAssets.text;
        //currentDialogue = new Dialogue(txtContents);
    }



    public void StartDialogue(string name, string[] sents)
    {
        sentences.Clear(); //clear previous conversation
        nameText.text = name;
        animator.SetBool("isOpen", true);

        foreach (string sentence in sents)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        string sentence = sentences.Dequeue();
        dialogueText.text = sentence;
        if (sentences.Count == 0)
        {
            choices.SetActive(true);
        }
    }

    void EndDialogue()
    {
        animator.SetBool("isOpen", false);
        choices.SetActive(false);
    }

    //public void ChangeHonor(int amount)
    //{
    //    honor += amount;
    //    honor = Mathf.Clamp(honor, 0, maxHonor); //clamps honor between 0 and max (100)

    //    honorFill.fillAmount = honor / maxHonor;
    //}

    public void ContinuePressed(GameObject button)
    {
        if (button.GetComponent<Image>().sprite == unpressedBtn)
            button.GetComponent<Image>().sprite = pressedBtn;
        else
            button.GetComponent<Image>().sprite = unpressedBtn;

        DisplayNextSentence();
    }
}
