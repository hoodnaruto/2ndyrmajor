﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour {

    public Image testGraphic;
    private IEnumerator coroutine;
	// Use this for initialization
	void Start () {
        SetAlpha(testGraphic.color, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		//if (testGraphic.color.a == 1f)
  //      {
            //Debug.Log(testGraphic.color.a);
            coroutine = FadeIn(testGraphic);
            //Debug.Log(coroutine);
            StartCoroutine(coroutine);
        //}
        //else if (testGraphic.color.a == 0f)
        //{
        //    coroutine = FadeOut(testGraphic);
        //    StartCoroutine(coroutine);
        //}
	}

    //public IEnumerator FadeIn(Image image, float speed = 1.0f, bool smooth = true)
    //{
    //    speed *= Time.deltaTime;
    //    //lerp them -> fade in new, fade out old
    //    image.GetComponent<Image>().color = SetAlpha(image.color, (smooth) ? Mathf.Lerp(image.GetComponent<Image>().color.a, 1f, speed) : Mathf.MoveTowards(image.GetComponent<Image>().color.a, 1f, speed));

    //    yield return new WaitForEndOfFrame();
    //}

    //public IEnumerator FadeOut(Image image, float speed = 1.0f, bool smooth = true)
    //{
    //    speed *= Time.deltaTime;
    //    //lerp them -> fade in new, fade out old
    //    image.GetComponent<Image>().color = SetAlpha(image.color, (smooth) ? Mathf.Lerp(image.GetComponent<Image>().color.a, 0f, speed) : Mathf.MoveTowards(image.GetComponent<Image>().color.a, 0f, speed));

    //    yield return new WaitForEndOfFrame();
    //}

    public IEnumerator FadeIn(Image image)
    {
        for (float f = 0f; f <= 1f; f+= 0.05f )
        {
            image.color = SetAlpha(image.color, f);
            Debug.Log(image.color.a);
            yield return new WaitForSeconds(0.05f);
        }
    }

    public IEnumerator FadeOut(Image image)
    {
        for (float f = 1.0f; f >= 0f; f -= 0.05f)
        {
            image.color = SetAlpha(image.color, f);
            Debug.Log(image.color.a);
            yield return new WaitForSeconds(0.05f);
        }
    }
    public static Color SetAlpha(Color color, float alpha)
    {
        return new Color(color.r, color.g, color.b, alpha);
    }
}