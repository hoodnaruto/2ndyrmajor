﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueSystem : MonoBehaviour
{
    public List<Sprite> backgrounds = new List<Sprite>(); //sets up backgrounds
    public List<Sprite> cinematics = new List<Sprite>();

    public List<string> charNames = new List<string>();
    public List<Color> charTextColours = new List<Color>();
    public List<Color> charTextBoxColours = new List<Color>();

    public List<AudioClip> clipsInScene = new List<AudioClip>();

    protected List<GameObject> characters = new List<GameObject>(); //will be automatically filled in, don't show in editor

    public string nextScene;

    public TextAsset importedText;

    public Dialogue dialogueScript;

    public bool isAutoPlaying;

    public List<Sprite> posesInCutscene = new List<Sprite>();

    public GameObject dialogueBox;

    public GameObject continueBtn;

    public GameObject optionsLayer;
    public GameObject backgroundLayer;
    public GameObject characterLayer;
    public GameObject cinematicLayer;

    public GameObject menuLayer;
    protected bool optionsActive;
    protected bool menuActive;
    public GameObject optLayer;

    public Color evilChoiceColor;
    public Color goodChoiceColor;

    //public float imageTransSpeed;
    //public bool smoothTransition;

    protected List<GameObject> optionsCol = new List<GameObject>(); //doesn't need to 
                                                                    //back does -2 on curNode then ProccessNode

    protected virtual void Start()
    {
        menuLayer.SetActive(false);
        menuActive = false;
        optionsActive = false;
        optLayer.SetActive(false);

    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!optionsActive)
            {
                ActivateMenu();
            }
            else
            {
                CloseOptions();
            }
        }
    }

    public void ActivateMenu()
    {
        if (menuActive)
        {
            menuActive = false;
            menuLayer.SetActive(false);
        }
        else
        {
            menuLayer.SetActive(true);
            menuActive = true;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ContinueGame()
    {
        ActivateMenu();
    }

    public void OpenOptions()
    {
        menuActive = false;
        optionsActive = true;
        optLayer.SetActive(true);
        menuLayer.SetActive(false);
    }

    public void CloseOptions()
    {
        menuActive = true;
        optionsActive = false;
        optLayer.SetActive(false);
        menuLayer.SetActive(true);
    }

    public void BackToMenu()
    {
        GameObject.Destroy(GlobalVariables.instance.gameObject);
        SceneManager.LoadScene("menuScene");
    }

    public void SetUp(string charPrefabLoc)
    {
        for (int i = 0; i < charNames.Count; i++)
        {
            GameObject go = Instantiate(Resources.Load(charPrefabLoc), characterLayer.transform) as GameObject; //make new sprite object at 
            go.GetComponent<Character>().color = charTextColours[i];
            go.GetComponent<Character>().name = charNames[i];
            go.GetComponent<Character>().textBoxColor = charTextBoxColours[i];
            go.name = charNames[i]; //change the new object's name to find it easier
            go.GetComponentInChildren<Image>().sprite = null;
            go.SetActive(false);
            characters.Add(go);
        }

        dialogueScript = new Dialogue(importedText);

        dialogueBox.SetActive(false);
    }

    protected virtual void ProcessLine(DialogueNode node) { }

    protected virtual void ContinueSpeech() { }

    protected string targetSpeech = "";
    protected Coroutine speaking = null;
    protected bool isSpeaking { get { return speaking != null; } }
    protected bool waitForTransition;

    [HideInInspector] public bool isWaitingForUserInput = false;
    public void StopSpeaking()
    {
        if (isSpeaking)
        {
            StopCoroutine(speaking);
            speaking = null;
            if (isAutoPlaying && continueBtn.activeInHierarchy) //if auto play is up and continue button has not been disabled, keep going
                StartCoroutine(LineTransition());
        }

    }

    protected IEnumerator LineTransition()
    {
        waitForTransition = true; //stops people just skipping through lines and throwing off everything
        yield return new WaitForSeconds(GlobalVariables.instance.LineTransitionTime);
        while (!isFading)
        {
            break;
        }
        waitForTransition = false;
        ContinueSpeech();
    }
    public virtual void Say(string speaker, string speech, bool dialogueOnBottom = false, bool additive = false) { }

    public GameObject CreateButton(string buttonName, int ln, bool isEvilChoice, int repImpact = 0, string flag = "")
    {
        //continueBtn.SetActive(false);
        GameObject button = Instantiate(Resources.Load("Button"), optionsLayer.transform) as GameObject;
        button.name = buttonName;
        button.transform.Find("Text").GetComponent<Text>().text = buttonName;
        button.GetComponent<Button>().onClick.AddListener(() => ChoiceClick(ln, repImpact));
        if (isEvilChoice)
        {
            button.transform.Find("Text").GetComponent<Text>().color = evilChoiceColor;
        }
        else
            button.transform.Find("Text").GetComponent<Text>().color = goodChoiceColor;
        return button;
    }

    public void ChoiceClick(int ln, int repImpact, string flag = "")
    {
        dialogueScript.curNode = ln;
        GlobalVariables.instance.reputation += repImpact;
        continueBtn.SetActive(true);
        if (flag != "") //if there's actually a flag to be raised
        {
            EventFlag flagFnd = GlobalVariables.instance.flags.Find(x => x.name == flag);
            if (flagFnd != null && flagFnd.isOn == true)
            {
                GlobalVariables.instance.lowerFlag(flag);
            }
            else
            {
                GlobalVariables.instance.raiseFlag(flag);
            }
        }
        foreach (GameObject go in optionsCol)
        {
            Destroy(go); //blow up all choice button once a choice has been made
        }
        ContinueSpeech();
    }

    public void ReturnBtn()
    {
        dialogueScript.curNode -= 1;
        if (dialogueScript.curNode <= 0)
            dialogueScript.curNode = 0; //lock it don't go out of bounds
        while (dialogueScript.curNode > 0) //don't bother going back if we're already at the start
        {
            DialogueNode node = dialogueScript.m_nodes.Find(x => x.nodeId == dialogueScript.curNode);
            if (node.m_options[0].optName.Contains("NEXT") == false)
            {
                dialogueScript.curNode -= 1;
            }
            else
            {
                break;
            }
        }
        ContinueSpeech();
    }

    public static Color SetAlpha(Color color, float alpha)
    {
        return new Color(color.r, color.g, color.b, alpha);
    }

    public void SetCG(string nwCG)
    {
        GameObject cgObject = cinematicLayer.transform.Find("CinematicGraphic").gameObject;

        Sprite newCG = cinematics.Find(x => x.name == nwCG + "_CG");

        if (newCG != null)
        {
            fading = StartCoroutine(FadeBg(cgObject.GetComponent<Image>(), true, newCG));
        }
        else
        {
            cgObject.GetComponent<Image>().color = SetAlpha(cgObject.GetComponent<Image>().color, 0f);
            cgObject.GetComponent<Image>().sprite = null;
        }
    }

    public void ChangeBackground(string newBg)
    {
        GameObject Background = backgroundLayer.transform.Find("Background").gameObject;

        Sprite bg = backgrounds.Find(x => x.name == newBg + "_Background");
        if (bg != null)
        {
            fading = StartCoroutine(FadeBg(Background.GetComponent<Image>(), true, bg));
        }
        else
        {
            Background.GetComponent<Image>().sprite = null;
        }
    }

    protected bool isFading { get { return fading != null; }  }
    protected Coroutine fading = null;
    private Image fadeImage = null;
    private Sprite fadeSprite = null;
    private Image fadeCharImage = null;
    private float targetX = -1;
    private float targetY = -1;

    public void StopFading()
    {
        if (isFading)
        {
            StopCoroutine(fading);
            fading = null;
        }
        if (fadeCharImage != null)
        {
            fadeCharImage.color = SetAlpha(fadeCharImage.color, 1f);
            if (targetX != -1 && targetY != -1)
            {
                RectTransform root = fadeCharImage.gameObject.transform.parent.GetComponent<RectTransform>();
                Vector2 padding = root.anchorMax - root.anchorMin;
                float maxX = 1f - padding.x;
                float maxY = 1f - padding.y;
                Vector2 minAnchorTarget = new Vector2(maxX * targetX, maxY * targetY);
                root.anchorMin = minAnchorTarget;
                root.anchorMax = root.anchorMin + padding;

                //set character in new position in case stop fading is called early
                targetX = -1;
                targetY = -1;
            }
            fadeCharImage = null;
        }
        if (fadeImage != null)
        {
            fadeImage.color = SetAlpha(fadeImage.color, 1f);
            if (fadeSprite != null)
            {
                fadeImage.sprite = fadeSprite;
                fadeSprite = null;
            }
            //replace sprite in case stop fading is called early
            fadeImage = null;
        }
    }
    public void MoveCharacter(string name, float tarX, float tarY, bool setUp)
    {
        GameObject chara = characters.Find(x => x.name == name);
        if (!chara.activeInHierarchy)
            chara.SetActive(true);
        if (!setUp)
        {
            StopFading(); //calls stop fading, needed to stop a weird flicker effect
            RectTransform root = chara.GetComponent<RectTransform>();
            fading = StartCoroutine(FadeChar(chara.transform.Find("SpriteRenderer").GetComponent<Image>(), true, root, tarX, tarY));
        }
        else
        {
            RectTransform root = chara.GetComponent<RectTransform>();

            Vector2 padding = root.anchorMax - root.anchorMin;
            float maxX = 1f - padding.x;
            float maxY = 1f - padding.y;
            Vector2 minAnchorTarget = new Vector2(maxX * tarX, maxY * tarY);
            root.anchorMin = minAnchorTarget;
            root.anchorMax = root.anchorMin + padding;
        }

    }

    public void SkipToLine(int newLine)
    {
        dialogueScript.curNode = newLine;
    }

    public void FlipCharacter(string name)
    {
        GameObject chara = characters.Find(x => x.name == name);
        if (!chara.activeInHierarchy)
            chara.SetActive(true);
        chara.transform.Rotate(new Vector3(0, 180, 0));
    }

    public void ResizeCharacter(string name, float tarWidth, float tarHeight)
    {
        GameObject chara = characters.Find(x => x.name == name);
        if (!chara.activeInHierarchy)
            chara.SetActive(true);
        GameObject spriteRender = chara.transform.Find("SpriteRenderer").gameObject;
        spriteRender.GetComponent<RectTransform>().sizeDelta = new Vector2(tarWidth, tarHeight);
    }

    public void PlayAudio(string clipName)
    {
        AudioClip clip = clipsInScene.Find(x => x.name == clipName);

        GlobalVariables.instance.source.PlayOneShot(clip); //playOneShot plays the called audio alongside the original audio.
    }

    public void ChangeMusic(string clipName)
    {
        AudioClip clip = clipsInScene.Find(x => x.name == clipName);

        AudioSource MusicSource = GlobalVariables.instance.source;
        MusicSource.Stop();
        if (clip != null)
        {
            MusicSource.clip = clip;
            MusicSource.Play();
        }
    }

    public IEnumerator FadeBg(Image image, bool fadeIn, Sprite newImage, float delay = 0.5f)
    {
        if (fadeIn)
        {
            fadeImage = image;
            fadeSprite = newImage;
            targetX = -1;
            targetY = -1;
            if (image.color.a >= 0.85f) //if it's faded in 
            {
                for (float f = image.color.a; f >= 0f; f -= 0.05f) //fade out 
                {
                    image.color = SetAlpha(image.color, f);
                    yield return new WaitForSeconds(0.05f);
                }
            }
            //replace
            if (newImage != null)
            {
                image.sprite = newImage;
            }

            for (float f = 0.05f; f <= 1f; f += 0.05f) //then fade in, otherwise, just fade in
            {
                image.color = SetAlpha(image.color, f);
                yield return new WaitForSeconds(0.05f);
            }
        }
        else
        {

            for (float f = 0.95f; f >= 0f; f -= 0.05f) //then fade out, otherwise, just fade out
            {
                image.color = SetAlpha(image.color, f);
                yield return new WaitForSeconds(0.05f);
            }
        }

        //yield return new WaitForSeconds(delay);
        StopFading();
    }

    public IEnumerator FadeChar(Image image, bool fadeIn, RectTransform root = null, float tarX = 0, float tarY = 0, float delay = 0.5f)
    {
        if (fadeIn)
        {
            fadeCharImage = image;
            targetX = tarX;
            targetY = tarY; //if stopFading gets called early, do this so it still gets to where needs

            if (image.color.a >= 0.85f) //if it's faded in 
            {
                for (float f = image.color.a; f >= 0f; f -= 0.05f) //fade out 
                {
                    image.color = SetAlpha(image.color, f);
                    yield return new WaitForSeconds(0.05f);
                }
            }

            Vector2 padding = root.anchorMax - root.anchorMin;
            float maxX = 1f - padding.x;
            float maxY = 1f - padding.y;
            Vector2 minAnchorTarget = new Vector2(maxX * tarX, maxY * tarY);
            root.anchorMin = minAnchorTarget;
            root.anchorMax = root.anchorMin + padding;

            for (float f = 0.05f; f <= 1f; f += 0.05f) //then fade in, otherwise, just fade in
            {
                image.color = SetAlpha(image.color, f);
                yield return new WaitForSeconds(0.05f);
            }
        }
        else
        {

            for (float f = 0.95f; f >= 0f; f -= 0.05f) //then fade out, otherwise, just fade out
            {
                image.color = SetAlpha(image.color, f);
                yield return new WaitForSeconds(0.05f);
            }
        }

        //yield return new WaitForSeconds(delay);
        StopFading();
    }

}


