﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    public string charName;
    public List<Sprite> poses;
    public Color color;
    public Color textBoxColor;

}
