﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVariables : MonoBehaviour {

    public static GlobalVariables instance;
    public int reputation;
    public float LineTransitionTime = 1f;
    public float talkingSpeed = 0.01f;
    public float volume;
    public AudioSource source;

    public List<EventFlag> flags = new List<EventFlag>();
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void raiseFlag(string flagName)
    {
        EventFlag flag = flags.Find(x => x.name == flagName);
        if (flag != null)
        {
            flag.isOn = true;
        }
        else
        {
            EventFlag newFlag = new EventFlag();
            newFlag.isOn = true;
            newFlag.name = flagName;
            flags.Add(newFlag);
        }
    }

    public void lowerFlag(string flagName)
    {
        EventFlag flag = flags.Find(x => x.name == flagName);
        if (flag != null)
        {
            flag.isOn = false;
        }
    }

    //public void checkFlag(string flagName)
    //{
    //    EventFlag flag = flags.Find(x => x.name == flagName);
    //  
    //}

}
