﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueOption
{
    public string optName;
    public int ln, honour;
    public bool isEvilChoice;
    public EventFlag flag;


    public DialogueOption(string text)
    {
        optName = text;
    } //as stage control
    public DialogueOption (string text, int line, int repImpact, bool isEvil, EventFlag eFlag)
    {
        //make button with same name as dialogue option
        optName = text;
        ln = line;
        honour = repImpact;
        isEvilChoice = isEvil;
        flag = eFlag;
    }

}
