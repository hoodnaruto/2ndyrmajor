﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameOver : MonoBehaviour {

    public void BackToMenu()
    {
        GameObject.Destroy(GlobalVariables.instance.gameObject);
        SceneManager.LoadScene("menuScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
