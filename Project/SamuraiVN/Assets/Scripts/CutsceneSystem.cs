﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//0,0 refers to bottom left - character x offset 0.1 - 1 y offset 0.3 - 0.8

public class CutsceneSystem : DialogueSystem
{
    protected override void Start()
    {
        base.Start();
        SetUp("Cutscene/Character");
        DialogueNode newNode = dialogueScript.m_nodes.Find(x => x.nodeId == dialogueScript.curNode);
        ProcessLine(newNode);
    }

    public override void Say(string speaker, string speech, bool dialogueOnBottom = false, bool additive = false)
    {
        StopSpeaking();
        GameObject speechBubble = characters.Find(x => x.name == speaker).transform.Find("SpeechBubble").gameObject;
        speechBubble.transform.Find("SpeechText").GetComponent<Text>().text = speech;
        speaking = StartCoroutine(Speaking(speech, additive, speaker, speechBubble, dialogueOnBottom));
    }

    IEnumerator Speaking(string speech, bool additive, string speaker, GameObject speechBubble, bool DialogueOnBottom)
    {
        if (!DialogueOnBottom)
        {
            dialogueBox.SetActive(false);
            speechBubble.SetActive(true);
            continueBtn = GameObject.Find("SpeechLayer").transform.Find("ContinueBtn").gameObject;
        }
        else
        {
            speechBubble.SetActive(false);
            dialogueBox.SetActive(true);
            continueBtn = dialogueBox.transform.Find("ContinueBtn").gameObject;
        }

        Text nameText;
        Text speechText;
        Color speechColor = speechBubble.transform.parent.GetComponent<Character>().color;
        Color textBoxColor = speechBubble.transform.parent.GetComponent<Character>().textBoxColor;
        if (!DialogueOnBottom)
        {
            nameText = speechBubble.transform.Find("Name").Find("NameText").GetComponent<Text>();
            speechText = speechBubble.transform.Find("SpeechText").GetComponent<Text>();
            speechBubble.GetComponent<Image>().color = textBoxColor;
        }
        else
        {
            nameText = dialogueBox.transform.Find("SpeakerNameBox").Find("SpeakerNameText").GetComponent<Text>();
            speechText = dialogueBox.transform.Find("SpeechBox").Find("SpeechText").GetComponent<Text>();
            dialogueBox.transform.Find("SpeechBox").GetComponent<Image>().color = textBoxColor;
        }
        speechText.color = speechColor;
        nameText.text = speaker;
        targetSpeech = speech;
        if (!additive)
            speechText.text = "";
        else
            speechText.text = speechText.text + targetSpeech;
        isWaitingForUserInput = false;
        while (speechText.text != targetSpeech) //text transition to simulate talking
        {
            speechText.text += targetSpeech[speechText.text.Length];
            if (!DialogueOnBottom)
            {
                speechBubble.transform.Find("SpeechReady").gameObject.GetComponent<Text>().text = "";
            }
            else
            {
                dialogueBox.transform.Find("ContinueBtn").Find("Text").GetComponent<Text>().text = "";
            }
            yield return new WaitForSeconds(GlobalVariables.instance.talkingSpeed);
        }

        if (!DialogueOnBottom)
        {
            speechBubble.transform.Find("SpeechReady").gameObject.GetComponent<Text>().text = ">>";
        }
        else
        {
            dialogueBox.transform.Find("ContinueBtn").Find("Text").GetComponent<Text>().text = ">>";
        }

        isWaitingForUserInput = true;
        while (!isWaitingForUserInput)
            yield return new WaitForEndOfFrame();

        StopSpeaking();
    }

    protected override void ProcessLine(DialogueNode newNode)
    {
        string name = newNode.m_name;
        string speech = newNode.m_text;
        string pose = newNode.m_pose;
        List<DialogueOption> options = newNode.m_options;
        string comm = options[0].optName;

        if (options.Count == 1)
        {
            foreach (GameObject chara in characters)
            {
                GameObject bubble = chara.transform.Find("SpeechBubble").gameObject;
                if (chara.name == name && speech != "")
                {
                    Image spriteImage = chara.transform.Find("SpriteRenderer").GetComponent<Image>();
                    if (spriteImage.color.a != 1f)
                    {
                        if (comm.Contains("BTM"))
                        {
                            spriteImage.color = SetAlpha(spriteImage.color, 1f);
                        }
                        else
                        {
                            spriteImage.color = SetAlpha(spriteImage.color, 1f);
                        }
                    }
                    if (comm.Contains("BTM"))
                    {
                        Say(name, speech, true);

                    }
                    else
                    {
                        Say(name, speech);
                    }
                }
                else
                {
                    bubble.SetActive(false);

                    if (comm.Contains("BTM"))
                    {
                        Image spriteImage = chara.transform.Find("SpriteRenderer").GetComponent<Image>();
                        spriteImage.color = SetAlpha(spriteImage.color, 0.3f);
                    }
                }
            }
            if (comm.Contains("CHGBG") && comm.Contains("MOV") && comm.Contains("FLIP") && 
                comm.Contains("RSZ") && comm.Contains("CHGCG") && comm.Contains("PLY") && comm.Contains("CHGMSC"))
            {
                Debug.LogAssertion("Type mismatch.");
            }

            if (comm.Contains("CHGBG"))
            {
                string command = options.Find(x => x.optName.Contains("CHGBG")).optName.Trim();
                string[] chg = command.Split('|');
                string newBg = chg[1];
                ChangeBackground(newBg);
                string newCg = "";
                SetCG(newCg);
            }

            if (comm.Contains("CHGCG"))
            {
                string command = options.Find(x => x.optName.Contains("CHGCG")).optName.Trim();
                string[] chg = command.Split('|');
                string newCg = chg[1];
                SetCG(newCg);
            }
            if (comm.Contains("PLY"))
            {
                string command = options.Find(x => x.optName.Contains("PLY")).optName.Trim();
                string[] chg = command.Split('|');
                string newClip = chg[1];
                PlayAudio(newClip);
            }
            if (comm.Contains("CHGMSC"))
            {
                string command = options.Find(x => x.optName.Contains("CHGMSC")).optName.Trim();
                string[] chg = command.Split('|');
                string newClip = chg[1];
                ChangeMusic(newClip);
            }
            if (comm.Contains("MOV"))
            {
                string command = options.Find(x => x.optName.Contains("MOV")).optName.Trim();
                string[] chg = command.Split('|');
                float tarX, tarY;
                float.TryParse(chg[1], out tarX);
                float.TryParse(chg[2], out tarY);
                if (comm.Contains("STUP"))
                {
                    MoveCharacter(name, tarX, tarY, true);
                }
                else
                {
                    MoveCharacter(name, tarX, tarY, false);
                }
                //string newCg = "";
                //SetCG(newCg);
            }

            if (comm.Contains("FLIP"))
            {
                FlipCharacter(name);
                //string newCg = "";
                //SetCG(newCg);
            }

            if (comm.Contains("RSZ"))
            {
                string command = comm.Trim();
                string[] chg = command.Split('|');
                float tarWidth, tarHeight;
                float.TryParse(chg[1], out tarWidth);
                float.TryParse(chg[2], out tarHeight);
                ResizeCharacter(name, tarWidth, tarHeight);
                //string newCg = "";
                //SetCG(newCg);
            }

            Sprite sprite = null;
            sprite = posesInCutscene.Find(x => x.name == name + "_" + pose + "_Sprite");
            GameObject spriteRender = characters.Find(x => x.name == name).transform.Find("SpriteRenderer").gameObject;

            if (sprite != null && spriteRender.GetComponent<Image>().sprite != sprite) //checks if the character's pose is the same as the newly designated pose
            {
                spriteRender.GetComponent<Image>().sprite = sprite;
                //spriteRender.GetComponent<Image>().color = SetAlpha(spriteRender.GetComponent<Image>().color, 0f);
                //StartCoroutine(Fade(spriteRender.GetComponent<Image>(), true));
                //no need for smoothness - see example 
            }
            else if (sprite == null)
            {
                characters.Find(x => x.name == name).SetActive(false); //turn off the character if the sprite is invalid
            }

            if (comm.Contains("SKIP"))
            {
                string command = options.Find(x => x.optName.Contains("SKIP")).optName.Trim();
                string[] chg = command.Split('|');
                int newLine;
                int.TryParse(chg[1], out newLine);
                SkipToLine(newLine);
            }

            if (!comm.Contains("NEXT") && !comm.Contains("SKIP"))
            {
                //do coroutine thing here
                if (options.Find(x => x.optName.Contains("STUP")) == null)
                    StartCoroutine(LineTransition());
                else
                    ContinueSpeech();
            }
        }
        else
        {
            //activate choice system
            if (options[0].optName.Contains("CHC"))
            {
                //disable ContinueBtn
                continueBtn.SetActive(false);
                for (int i = 1; i < options.Count; i++)
                {
                    string[] chc = options[i].optName.Split('|');
                    int ln, repImpact;
                    bool isEvil;
                    int.TryParse(chc[1], out ln);
                    int.TryParse(chc[2], out repImpact); //reminder to shove flags here
                    bool.TryParse(chc[3], out isEvil);
                    GameObject button = CreateButton(chc[0], ln, isEvil, repImpact);
                    optionsCol.Add(button);
                    Debug.Log(ln + repImpact);
                    Debug.Log(isEvil);
                    //place the buttons - scaling issues still may exist
                    string choiceIndex = "Choice" + i;
                    RectTransform choiceRef = GameObject.Find(choiceIndex).gameObject.GetComponent<RectTransform>();
                    button.GetComponent<RectTransform>().offsetMin = choiceRef.offsetMin;
                    button.GetComponent<RectTransform>().offsetMax = choiceRef.offsetMax;
                    button.GetComponent<RectTransform>().anchorMin = choiceRef.anchorMin;
                    button.GetComponent<RectTransform>().anchorMax = choiceRef.anchorMax;
                }

            }
        }
    }

    public void ContinueBtnPressed()
    {
        if (isAutoPlaying || isFading || menuActive) //disable click when auto-play is on
            return;

        ContinueSpeech();
    }

    public void AutoSelect(Toggle toggle)
    {
        isAutoPlaying = toggle.isOn;
        if (isAutoPlaying)
            ContinueSpeech();
    }

    protected override void ContinueSpeech()
    {
        Debug.Log(dialogueScript.curNode);
        if (isSpeaking)
        {
            DialogueNode curNode = dialogueScript.m_nodes.Find(x => x.nodeId == dialogueScript.curNode);
            string finalText = curNode.m_text;
            Text speechText;
            if (curNode.m_options.Find(x => x.optName.Contains("BTM")) != null)
            {
                speechText = dialogueBox.transform.Find("SpeechBox").Find("SpeechText").GetComponent<Text>();
                dialogueBox.transform.Find("ContinueBtn").Find("Text").GetComponent<Text>().text = ">>";

            }
            else
            {
                speechText = characters.Find(x => x.name == curNode.m_name).transform.Find("SpeechBubble").Find("SpeechText").GetComponent<Text>();
                characters.Find(x => x.name == curNode.m_name).transform.Find("SpeechBubble").Find("SpeechReady").GetComponent<Text>().text = ">>";
            }
            speechText.text = finalText;
            StopSpeaking();
        } //finish previous dialogue before continuing
        else if (!waitForTransition)
        {
            DialogueNode node = dialogueScript.m_nodes.Find(x => x.nodeId == dialogueScript.curNode);
            List<DialogueOption> options = node.m_options;
                if (options.Find(x => x.optName.Contains("END")) == null) //if it isn't end, continue to next line
                {
                    //check if it doesn't go out of bounds
                    if (dialogueScript.curNode >= dialogueScript.m_nodes.Count)
                    {
                        Debug.Log("Accessing undefined nodes");
                    }
                    else
                    {
                        //go to next text
                        dialogueScript.curNode++;
                        DialogueNode newNode = dialogueScript.m_nodes.Find(x => x.nodeId == dialogueScript.curNode);
                        //display name and text
                        ProcessLine(newNode);
                    }

                }
                else if (options.Find(x => x.optName.Contains("END")) != null)
                {
                    //end dialogue
                    //move to next scene
                    if (nextScene != "")
                        SceneManager.LoadScene(nextScene);
                }
            }
        }

    }