﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueNode {

    public int nodeId;
    public string m_name;
    public string m_pose;
    public string m_text;
    public List<DialogueOption> m_options = new List<DialogueOption>();

    public DialogueNode(string data)
    {
        //break it down here 
        //what to do about cutscenes and effects
        //special tag on name, like END and NEXT, that concels out rest of the 

        //definitely find a tut on visual novels
        string[] dataCol = data.Split(':');
        if (dataCol[0] != "EFFECT")
        {
            m_name = dataCol[0].Trim(); //get rid of any odd symbols caused new lines - this is first so entry so only necessary to do it here
            //string poseString = dataCol[1].ToString();
            //int.TryParse(poseString, out m_pose);
            m_pose = dataCol[1];
            m_text = dataCol[2];
            for (int i = 3; i < dataCol.Length; i++)
            {
                DialogueOption newOption = new DialogueOption(dataCol[i]); //end means end of branch ->game over, most likely; next means only has continue as option //process it in dialogue options
                m_options.Add(newOption);
            }
        }

    }

    //public string GetName()
    //{
    //    return m_name;
    //}

    //public string GetPose()
    //{
    //    return m_pose;
    //}

    //public string GetText()
    //{
    //    return m_text;
    //}

    //public List<DialogueOption> GetChoices()
    //{
    //    return m_options;
    //}
  
}
