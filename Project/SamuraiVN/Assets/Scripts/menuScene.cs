﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.UI;

public class menuScene : MonoBehaviour {

    public GameObject preparingImage;
    public List<VideoPlayer> players;
    public List<VideoClip> clips;
    public GameObject playButton;
    public GameObject exitButton;
    int videoIndex = 0;

	// Use this for initialization
	void Start ()
    {
        playButton.SetActive(false);
        exitButton.SetActive(false);
        StartCoroutine(PrepareVideos());
        players[videoIndex].enabled = true;
        players[videoIndex].isLooping = true;
        players[videoIndex].Play();
    }
	

    public void StartGame()
    {
        players[videoIndex].Stop();
        players[videoIndex].enabled = false;
        videoIndex++;
        players[videoIndex].enabled = true;
        players[videoIndex].Play();
        StartCoroutine(waitForMovieEnd(1.9f));
        //Begin_clip.enabled = true;
        //StartCoroutine(DelayVideoPlayback());

        //Start_Clip.enabled = false;
        //StartCoroutine(waitForMovieEnd(2.5f));
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    IEnumerator DelayVideoPlayback()
    {
        const int numFramesToDelay = 10;
        for (int i = 0; i < numFramesToDelay; i++)
        {
            yield return null;
        }
    }

    IEnumerator waitForMovieEnd(float value)
    {
        yield return new WaitForSeconds(value);
        SceneManager.LoadScene("prologScene");
    }

    IEnumerator PrepareVideos()
    {
        if (clips == null || clips.Count <= 0)
        {
            Debug.LogError("Assign Video Clips from the editor");
            yield break;
        }

        if (players.Count != clips.Count)
        {
            Debug.LogError("Number of clips do not match number of players");
            yield break;
        }

        for (int i = 0; i < players.Count; i++)
        {
            players[i].playOnAwake = false;
            players[i].isLooping = false;
            players[i].clip = clips[i];
            players[i].Prepare();
            while (!players[i].isPrepared)
            {
                yield return null;
            }
        }
        for (float f = 0.95f; f >= 0f; f -= 0.05f) //fade out preparing image
        {
            preparingImage.GetComponent<Image>().color = SetAlpha(preparingImage.GetComponent<Image>().color, f);
            yield return new WaitForSeconds(0.05f);
        }
        playButton.SetActive(true);
        exitButton.SetActive(true);
        preparingImage.SetActive(false);
    }

    public static Color SetAlpha(Color color, float alpha)
    {
        return new Color(color.r, color.g, color.b, alpha);
    }

}
