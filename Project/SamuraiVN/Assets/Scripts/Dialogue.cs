﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue {
    //name:expression:text:options
    public List<DialogueNode> m_nodes = new List<DialogueNode>();
    public int listNodes; //tracks how many lines are in the scene in total
    public int curNode; //tracks which line we are currently reading

    public Dialogue(TextAsset import)
    {
        string dataSource = import.text;
        listNodes = 0;
        string[] dataChunks = dataSource.Split(';');
        foreach (string str in dataChunks)
        {
            DialogueNode node = new DialogueNode(str);
            node.nodeId = listNodes;
            Debug.Log(listNodes);
            listNodes++;
            m_nodes.Add(node);
        }
        curNode = 0;
    }


    public Dialogue(string dataSource)
    {
        listNodes = 0;
        string[] dataChunks = dataSource.Split(';');
        foreach (string str in dataChunks)
        {
            DialogueNode node = new DialogueNode(str);
            node.nodeId = listNodes;
            listNodes++;
            m_nodes.Add(node);
        }
        curNode = 0;
    }


}
