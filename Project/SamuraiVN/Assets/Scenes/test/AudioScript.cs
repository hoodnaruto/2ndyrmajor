﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour {

    public List<AudioClip> MusicClips = new List<AudioClip>();
    public AudioSource MusicSource;
	// Use this for initialization
    public void PlayButtonClicked()
    {
        Debug.Log("Clicked");
        AudioClip MusicClip = MusicClips.Find(x => x.name == "Intense_Track_Ver1");
        MusicSource.Stop();
        MusicSource.clip = MusicClip;
        MusicSource.Play();
        //MusicSource.PlayOneShot(MusicClip);
    }
}
