﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    public Vector3 offset;

	// Use this for initialization
	void Start () {
        //offset.z = this.transform.position.z - target.position.z;
        offset.y = this.transform.position.y - target.position.y;
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.position = target.position + offset;
        this.transform.LookAt(target.transform);
        //this.transform.rotation = player.rotation;
    }
}
