﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharMove : MonoBehaviour {

    public float moveSpeed = 200.0f;
    public float turnSpeed = 60.0f;

    public CharacterController controller;

    public DialogueManager dm;

    // Use this for initialization
    void Start () {
        dm = FindObjectOfType<DialogueManager>();
	}
	
	// Update is called once per frame
	void Update () {
        //if (dm.animator.GetBool("isOpen") == false) //freezes character while dialogue is happening
        //{
            float vert = Input.GetAxis("Vertical");
            float hor = Input.GetAxis("Horizontal");

            controller.SimpleMove(transform.forward * vert * moveSpeed * Time.deltaTime);
            this.transform.Rotate(transform.up, hor * moveSpeed * Time.deltaTime);
        //}

    }
}
