﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue : MonoBehaviour {

    public string name;
    [TextArea(3, 10)]
    public string[] sentences;

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(name, sentences);
        GameObject choices = FindObjectOfType<DialogueManager>().choices;
        if (choices.activeInHierarchy == true)
        {
            choices.SetActive(false);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            TriggerDialogue();
        }
    }

    public void ChoiceSelected(GameObject button)
    {
        if (button.name == "Button1")
        {
            FindObjectOfType<DialogueManager>().ChangeHonor(+10);
        }
        else if (button.name == "Button2")
        {
            FindObjectOfType<DialogueManager>().ChangeHonor(-10);
        }
        TriggerDialogue();
    }
}
